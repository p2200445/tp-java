import javax.print.Doc;

public class DocBibliotheque {
    private String code;
    private String title;
    private String author;
    private int year;
    private String statut;
    public static int nbrEmprunt = 0;
    public static int nbrRetour = 0;
    public static int nbrReservation = 0;
    private MembreBibliotheque emprunteur;
    private MembreBibliotheque reserveur;
    private Livre livre;
    private DocURL docURL;
    private CD cd;

    // Constructors
    public DocBibliotheque(){
        code = "N/A";
        title = "N/A";
        author = "N/A";
        year = 0;
        emprunteur = null;
        reserveur = null;
    }
    public DocBibliotheque(String newCode, String newTitle, String newAuthor, int newYear){
        this.code = newCode;
        this.title = newTitle;
        this.author = newAuthor;
        this.year = newYear;
        this.statut = "etagere";
    }

    // Accessors
    public static int getNbrEmprunt() {
        return nbrEmprunt;
    }
    public static int getNbrRetour() {
        return nbrRetour;
    }
    public static int getNbrReservation() {
        return nbrReservation;
    }
    public String getCode() {
        return code;
    }
    public String getTitle() {
        return title;
    }
    public String getAuthor() {
        return author;
    }
    public int getYear() {
        return year;
    }
    public String getStatut(){
        return this.statut;
    }
    public MembreBibliotheque getEmprunteur(){
        if (!isEmprunt()) {
            return null;
        } else {
            return emprunteur;
        }
    }
    public MembreBibliotheque getReserveur(){
        if (!isReservation()){
            return null;
        } else {
            return reserveur;
        }
    }
    // Modifiers
    public void setCode(String code) {
        this.code = code;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public void setStatut(String statut) {
        this.statut = statut;
    }
    public void setEmprunteur(MembreBibliotheque E){
        this.emprunteur = E;
    }
    public void setReserveur(MembreBibliotheque R){
        this.reserveur = R;
    }

    //Méthodes
    public boolean empruntDoc(MembreBibliotheque membre) throws EmpruntException{
        boolean isEmprunt = false;
        if (membre != null) {
            if (this.statut.equals("etagere")) {
                setStatut("emprunt");
                setEmprunteur(membre);
                isEmprunt = true;
                nbrEmprunt++;
            } else if (this.statut.equals("reservation")) {
                setStatut("emprunt");
                setEmprunteur(membre);
                isEmprunt = true;
                nbrReservation--;
                nbrEmprunt++;
            } else {
                throw new EmpruntException();
            }
        }
        return isEmprunt;
    }
    public boolean retourDoc(MembreBibliotheque membre){
        boolean isRetour = false;
        if (membre != null) {
            if ((this.statut.equals("emprunt")) && (this.emprunteur == membre)) {
                setStatut("retour");
                setEmprunteur(null);
                isRetour = true;
                nbrRetour++;
                nbrEmprunt--;
            } else if ((this.statut.equals("reservation")) && (this.reserveur == membre)) {
                setStatut("retour");
                setReserveur(null);
                isRetour = true;
                nbrReservation--;
                nbrRetour++;
            }
        }
        return isRetour;
    }
    public boolean reservationDoc(MembreBibliotheque membre){
        boolean isReservation = false;
        if (membre != null) {
            if (this.statut.equals("emprunt")) {
                setStatut("reservation");
                setEmprunteur(null);
                setReserveur(membre);
                nbrReservation++;
                nbrEmprunt--;
            }
        }
        return isReservation;
    }
    public boolean annuleReservationDoc(MembreBibliotheque membre){
        boolean isAnnule = false;
        if (membre != null) {
            if ((this.statut.equals("emprunt")) && (this.emprunteur == membre)) {
                setStatut("retour");
                setEmprunteur(null);
                isAnnule = true;
                nbrEmprunt--;
                nbrRetour++;
            } else if ((this.statut.equals("reservation")) && (this.reserveur == membre)) {
                    setStatut("retour");
                    setReserveur(null);
                    nbrRetour++;
                    nbrReservation--;
                    isAnnule = true;
                }
        }
        return isAnnule;
    }
    public boolean remiseDoc(){
        boolean isRemise = false;
        if (this.statut.equals("retour")){
            setStatut("etagere");
            isRemise = true;
            nbrRetour--;
        }
        return isRemise;
    }
    public boolean isEmprunt(){
        return this.statut.equals("emprunt");
    }
    public boolean isRetour(){
        return this.statut.equals("retour");
    }
    public boolean isReservation(){
        return this.statut.equals("reservation");
    }
    public boolean isEtagere(){
        return this.statut.equals("etagere");
    }

    @Override
    public String toString() {
        return "DocBibliotheque{" +
                "Code d'archivage ='" + code + '\'' +
                ", Titre ='" + title + '\'' +
                ", Auteur ='" + author + '\'' +
                ", Année =" + year +
                ", Statut ='" + statut + '\'' +
                ", Emprunteur =" + emprunteur +
                ", Reserveur =" + reserveur +
                '}';
    }
}
