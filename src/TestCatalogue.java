public class TestCatalogue {
    static DocBibliotheque D1 = new DocBibliotheque("004.178 K20PM", "Introduction à Java", "J.Leblanc", 2015);
    static DocBibliotheque D2 = new DocBibliotheque("967.4987 T248O", "Structures de données", "M.Machin", 2022);
    static DocBibliotheque D3 = new DocBibliotheque("12.12 SOT12", "Stamp on the 12", "A.Raimondouze", 2012);
    static DocBibliotheque D4 = new DocBibliotheque("123.456 ABCD123", "Graphes", "C.Jalloux", 2023);
    static Livre L1 = new Livre("102.203.67", "The Hobbit", "J.Tolkien", 2004, 600, "Haribo", "4/5");
    static Livre L2 = new Livre("876.123 OPMN2", "Harry Potter", "J.K.Rowling", 2012, 4000, "HogwartsProd", "5/5");
    static MembreBibliotheque M1 = new MembreBibliotheque("Jean", "Pierre",784381938, "1 rue de la rue");
    static MembreBibliotheque M2 = new MembreBibliotheque("Louka", "Lemarchand", 121212121, "12 rue de la douzaine");
    static CatalogueBibliotheque C1 = new CatalogueBibliotheque();
    static CatalogueBibliotheque C2 = new CatalogueBibliotheque();
    public static void main(String[] args) {
        C1.ajDoc(D1);
        C1.ajDoc(D2);
        C2.ajDoc(D3);
        C2.ajDoc(D4);
        C1.emprunteDoc(0, L1);
        C1.emprunteDoc(1, M2);
        C2.emprunteDoc(0, M1);
        C2.emprunteDoc(1, M2);
        C1.reserveDoc(0, M1);
        C2.rendreDoc(0, M1);
        C2.rendreDoc(1, M2);
        C2.remiseDoc(0);
        System.out.println(C1.accesDoc(0));
        System.out.println(C2.accesDoc(0));
        System.out.println(C1.accesDoc(1));
        System.out.println(C2.accesDoc(1));
    }
}
