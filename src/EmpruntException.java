public class EmpruntException extends Exception {
    public EmpruntException(){
        super("Emprunt impossible");
    }
}
