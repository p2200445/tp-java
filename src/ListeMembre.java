import java.util.ArrayList;

public class ListeMembre {
    private final ArrayList<MembreBibliotheque> listeMembre;
    public ListeMembre(){
        this.listeMembre = new ArrayList<>();
    }
    public boolean ajMembre(MembreBibliotheque membreAj){
        boolean isAjout = false;
        if (!this.listeMembre.contains(membreAj) && membreAj != null) {
            this.listeMembre.add(membreAj);
            isAjout = true;
        }
        return isAjout;
    }
    public MembreBibliotheque accesMembre(int numabonne) {
        int indice = 0;
        int i = 0;
        while (i < this.listeMembre.size()){
            if(this.listeMembre.get(i).getNumAbonne() == numabonne){
                indice = i;
            }
            i++;
        }
        return this.listeMembre.get(indice);
    }
    public void affichetoutlesMembres() {
        for (MembreBibliotheque membreBibliotheque : listeMembre) {
            System.out.println(membreBibliotheque.toString());
        }
        if (this.listeMembre.size() < 1) {
            System.out.println("Il n'y a rien dans cette liste");
        }
    }
}
