public class DocURL extends DocBibliotheque{
    private String url;
    private String description;
    public DocURL(String newCodearchivage, String newTitre, String newAuteur, int newAnnee, int newStatus, String newURL ,String newDesc) {
        super(newCodearchivage, newTitre, newAuteur, newAnnee, newStatus);
        this.url = newURL;
        this.description = newDesc;
    }
    public String getUrl(){
        return this.url;
    }
    public String getDesc(){
        return this.description;
    }
    public void setUrl(String url){
        this.url = url;
    }
    public void setDescription(String desc){
        this.description = desc;
    }
    public boolean empruntDoc(MembreBibliotheque membre){
        System.out.println("Impossible d'emprunter un document Digital.");
        return false;
    }
    public boolean reservationDoc(MembreBibliotheque membre){
        System.out.println("Impossible de reserver un document Digital.");
        return false;
    }
    public boolean retourDoc(MembreBibliotheque membre){
        System.out.println("Impossible de retourner un document Digital.");
        return false;
    }
    public boolean annuleReservationDoc(MembreBibliotheque membre){
        System.out.println("Impossible d'annuler une reservation pour un document Digital.");
        return false;
    }
    public boolean remiseDoc(){
        System.out.println("Impossible de remettre un document Digital sur l'etagere.");
        return false;
    }
}
