public class Livre extends DocBibliotheque {
    private double nombrePages;
    private String editeur;
    private String ISBN;
    public Livre(String newCode, String newTitle, String newAuthor, int newYear, double newNbrPages, String newEditeur, String newISBN){
        super(newCode, newTitle, newAuthor, newYear);
        this.nombrePages = newNbrPages;
        this.editeur = newEditeur;
        this.ISBN = newISBN;
    }
    public String getISBN() {
        return ISBN;
    }
    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }
    public String getEditeur() {
        return editeur;
    }
    public void setEditeur(String editeur) {
        this.editeur = editeur;
    }
    public double getNombrePages() {
        return nombrePages;
    }
    public void setNombrePages(double nombrePages) {
        this.nombrePages = nombrePages;
    }

    @Override
    public String toString() {
        return "Livre{" +
                super.toString()+
                "nombrePages=" + nombrePages +
                ", editeur='" + editeur + '\'' +
                ", ISBN='" + ISBN + '\'' +
                '}';
    }
}
