public class CD extends DocBibliotheque{
    private String listeMorceaux;
    public CD(){
        listeMorceaux ="N/A";
    }
    public CD(String newCode, String newTitle, String newAuthor, int newYear, String newListMorceaux){
        super(newCode, newTitle, newAuthor, newYear);
        this.listeMorceaux = newListMorceaux;
    }
    public String getListeMorceaux() {
        return listeMorceaux;
    }
    public void setListeMorceaux(String listeMorceaux) {
        this.listeMorceaux = listeMorceaux;
    }
}
