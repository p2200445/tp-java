public class MembreBibliotheque {
    private String nom;
    private String prenom;
    private double numTel;
    private String adresse;
    private static int numAbonne;
    private static int numAboTemp = 0;

    public MembreBibliotheque(){
        nom = "N/A";
        prenom = "N/A";
        numTel = 0;
        adresse = "N/A";
        numAbonne = 0;
    }
    public MembreBibliotheque(String newNom, String newPrenom, double newNumTel, String newAdresse){
        this.nom = newNom;
        this.prenom = newPrenom;
        this.numTel = newNumTel;
        this.adresse = newAdresse;
        this.numAbonne = numAboTemp++;
    }
    public static int getNumAbonne() {
        return numAbonne;
    }
    public double getNumTel() {
        return numTel;
    }
    public String getAdresse() {
        return adresse;
    }
    public String getNom() {
        return nom;
    }
    public String getPrenom() {
        return prenom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
    public static void setNumAbonne(int numAbonne) {
        MembreBibliotheque.numAbonne = numAbonne;
    }
    public void setNumTel(double numTel) {
        this.numTel = numTel;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Override
    public String toString() {
        return "MembreBibliotheque{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", numTel=" + numTel +
                ", adresse='" + adresse + '\'' +
                ", numAbonne='"+ numAbonne + '\'' +
                '}';
    }
}
