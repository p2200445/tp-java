import java.util.Scanner;

public class TestBibliotheque {
    static DocBibliotheque D1 = new DocBibliotheque("004.178 K20PM", "Introduction à Java", "J.Leblanc", 2015);
    static DocBibliotheque D2 = new DocBibliotheque("967.4987 T248O", "Structures de données", "M.Machin", 2022);
    static DocBibliotheque D3 = new DocBibliotheque("12.12 SOT12", "Stamp on the 12", "A.Raimondouze", 2012);
    static DocBibliotheque D4 = new DocBibliotheque("123.456 ABCD123", "Graphes", "C.Jalloux", 2023);
    static Livre L1 = new Livre("102.203.67", "The Hobbit", "J.Tolkien", 2004, 600, "Haribo", "4/5");
    static Livre L2 = new Livre("876.123 OPMN2", "Harry Potter", "J.K.Rowling", 2012, 4000, "HogwartsProd", "5/5");
    static MembreBibliotheque M1 = new MembreBibliotheque("Jean", "Pierre",784381938, "1 rue de la rue");
    static MembreBibliotheque M2 = new MembreBibliotheque("Louka", "Lemarchand", 121212121, "12 rue de la douzaine");
    public static void main(String[] args) {
        D1.empruntDoc(M1);
        D2.empruntDoc(M2);
        D3.empruntDoc(M1);
        D4.empruntDoc(M2);
        D1.reservationDoc(M1);
        D3.retourDoc(M1);
        D4.retourDoc(M2);
        D3.remiseDoc();
        System.out.println(D1);
        System.out.println(D2);
        System.out.println(D3);
        System.out.println(D4);
        System.out.println(L1);
        System.out.println(L2);

    }

}
