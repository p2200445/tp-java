import java.util.Scanner;

public class TestMenuBibliotheque {
    private static Scanner entree = new Scanner (System.in);
    private static DocBibliotheque choixDocument(){
        int opt;
        DocBibliotheque d=null;
        do {
            System.out.println("Choisissez : ");
            System.out.println("1 - "+D1.getTitle());
            System.out.println("2 - "+D2.getTitle());
            System.out.println("3 - "+D3.getTitle());
            System.out.println("4 - "+D4.getTitle());
            opt = entree.nextInt();
            switch (opt) {
                case 1 -> d = D1;
                case 2 -> d = D2;
                case 3 -> d = D3;
                case 4 -> d = D4;
                default -> System.out.println("Option Invalide, veuillez réessayer :");
            }
        } while (opt <= 0 || opt > 4);
        return d;
    }
    private static MembreBibliotheque choixMembre(){
        int opt;
        MembreBibliotheque m=null;
        do {
            System.out.println("Choisissez : ");
            System.out.println("1 - Jean");
            System.out.println("2 - Louka");
            opt = entree.nextInt();
            switch (opt) {
                case 1 -> m = M1;
                case 2 -> m = M2;
                default -> System.out.println("Option Invalide, veuillez réessayer :");
            }
        } while ((opt!=1) && (opt!=2));
        return m;
    }
    static DocBibliotheque D1 = new DocBibliotheque("004.178 K20PM", "Introduction à Java", "J.Leblanc", 2015);
    static DocBibliotheque D2 = new DocBibliotheque("967.4987 T248O", "Structures de données", "M.Machin", 2022);
    static DocBibliotheque D3 = new DocBibliotheque("12.12 SOT12", "Stamp on the 12", "A.Raimondouze", 2012);
    static DocBibliotheque D4 = new DocBibliotheque("123.456 ABCD123", "Graphes", "C.Jalloux", 2023);
    static MembreBibliotheque M1 = new MembreBibliotheque("Jean", "Pierre",784381938, "1 rue de la rue");
    static MembreBibliotheque M2 = new MembreBibliotheque("Louka", "Lemarchand", 121212121, "12 rue de la douzaine");
    public static void main(String[] args) {
        int menuOption;
        MembreBibliotheque M;
        DocBibliotheque D;
        do {
            System.out.println("\n       Options du Menu");
            System.out.println(" 1 - Affichage des caractéristiques d'un Document");
            System.out.println(" 2 - Affichage du statut d'un document");
            System.out.println(" 3 - Emprunt d'un document");
            System.out.println(" 4 - Reservation d'un document");
            System.out.println(" 5 - Retour d'un document");
            System.out.println(" 6 - Annuler la reservation d'un document");
            System.out.println(" 7 - Remise d'un livre sur l'étagère");
            System.out.println(" 8 - Affichage de nombre de documents empruntés");
            System.out.println(" 9 - Affichage du nombre de documents sur la pile des retours");
            System.out.println("10 - Affichage du nombre de documents dans la section réservation");
            System.out.println(" 0 - Quitter le menu");
            menuOption = entree.nextInt();
            entree.nextLine();
            if (menuOption != 0) {
                switch (menuOption) {
                    case 1 -> {
                        D = choixDocument();
                        System.out.println(D);
                    }
                    case 2 -> {
                        D = choixDocument();
                        System.out.println(D.getStatut());
                    }
                    case 3 -> {
                        D = choixDocument();
                        M = choixMembre();
                        if (D.empruntDoc(M)) {
                            System.out.println("Document emprunté !");
                        } else {
                            System.out.println("Echec de l'emprunt !");
                        }
                    }
                    case 4 -> {
                        D = choixDocument();
                        M = choixMembre();
                        if (D.reservationDoc(M)) {
                            System.out.println("Document réservé !");
                        } else {
                            System.out.println("Echec de la réservation !");
                        }
                    }
                    case 5 -> {
                        D = choixDocument();
                        M = choixMembre();
                        if (D.retourDoc(M)) {
                            System.out.println("Document retourné !");
                        } else {
                            System.out.println("Echec du retour !");
                        }
                    }
                    case 6 -> {
                        D = choixDocument();
                        M = choixMembre();
                        if (D.annuleReservationDoc(M)) {
                            System.out.println("Reservation annulée !");
                        } else {
                            System.out.println("Echec de l'annulation !");
                        }
                    }
                    case 7 -> {
                        D = choixDocument();
                        if (D.remiseDoc()) {
                            System.out.println("Document remis en étagère !");
                        } else {
                            System.out.println("Echec de la remise en étagère !");
                        }
                    }
                    case 8 -> System.out.println("Nombre d'emprunts = " + DocBibliotheque.nbrEmprunt);
                    case 9 -> System.out.println("Nombre de reservation = " + DocBibliotheque.nbrReservation);
                    case 10 -> System.out.println("Nombre de retours = " + DocBibliotheque.nbrRetour);

                    default -> System.out.println("Option invalide, faites un nouveau choix");
                    }
                }
            } while (menuOption != 0);
        }
    }
