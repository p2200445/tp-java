import java.util.ArrayList;
import java.util.Objects;

public class CatalogueBibliotheque {
    private final ArrayList<DocBibliotheque> docBibliothequeArrayList;
    public CatalogueBibliotheque(){
        this.docBibliothequeArrayList = new ArrayList<>();
    }
    public boolean ajDoc(DocBibliotheque docAj){
        return this.docBibliothequeArrayList.add(docAj);
    }
    public boolean supDoc(DocBibliotheque supDoc){
    return this.docBibliothequeArrayList.remove(supDoc);
    }
    public DocBibliotheque accesDoc(int i){
        DocBibliotheque document = null;
        if(i >= 0 && i < this.docBibliothequeArrayList.size()){
            document = this.docBibliothequeArrayList.get(i);
        }
        return document;
    }
    public void afficheTousLesDocs(){
        for (int i = 0; i < this.docBibliothequeArrayList.size(); i++){
            System.out.println(i +" ."+ this.docBibliothequeArrayList.get(i).getTitle());
        }
        if (this.docBibliothequeArrayList.size() < 1){
            System.out.println("Il n'y a rien dans ce docBibliothequeArrayList");
        }
    }
    public boolean emprunteDoc(int indiceDoc, MembreBibliotheque m) throws EmpruntException{
        if (this.docBibliothequeArrayList.get(indiceDoc).empruntDoc(m)){
            return this.docBibliothequeArrayList.get(indiceDoc).empruntDoc(m);
        } else {
            throw new EmpruntException();
        }
    }
    public boolean reserveDoc(int indiceDoc, MembreBibliotheque m){
        return this.docBibliothequeArrayList.get(indiceDoc).reservationDoc(m);
    }
    public boolean annulResaDoc(int indiceDoc, MembreBibliotheque m){
        return this.docBibliothequeArrayList.get(indiceDoc).annuleReservationDoc(m);
    }
    public boolean rendreDoc(int indiceDoc, MembreBibliotheque m){
        return this.docBibliothequeArrayList.get(indiceDoc).retourDoc(m);
            }
    public boolean remiseDoc(int indiceDoc){
        return this.docBibliothequeArrayList.get(indiceDoc).remiseDoc();
            }

}
